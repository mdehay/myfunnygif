import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter/material.dart';

import 'models/Giffy.dart';


/* Voici mon environnement de test a propos de HIVE
*
* Il s'agit ici à la base d'un test pour remplacer le SQFLite
* Si les premiers tests fonctionnent sur une sessions j'ai vite eu un soucis vis à vis du model
* En effet si hivebox.put arrive à me prendre des Key,Value , il n'arrive pas à me prendre un Key,Objet soit mon objet ici est Giffy
* Je laisse le page de test du hive montrant que j'ai voulu essayer cette methode, mais qu'elle a une erreur sans que je comprenne pourquoi
* J'ai aussi laissé mon model "Giffy" avec le hive
* */

class HiveMVC extends StatefulWidget {
  @override
  _HiveMVCState createState() => _HiveMVCState();
}

class _HiveMVCState extends State<HiveMVC> {
  Hivetest test = new Hivetest();

  @override
  Widget build(BuildContext context) {

    return Center(
      child: FutureBuilder(
        future: test.open(),
        builder: (context, snapshot) {
          if(snapshot.hasData){
            return RaisedButton(
              onPressed: () => test.open(),
              child: Text("${snapshot.data.length}"),
            );
          }else if(snapshot.hasError){
            print(snapshot.error);
            return Text("ERROR");
          }else{
            return Text("WAITING");
          }
        },
      ),
    );
  }
}

class Hivetest {
  Box<dynamic> box;

  Future<Box<dynamic>> open() async {
    await Hive.initFlutter();
    box = await Hive.openBox('giffyBox');
    Giffy giffy = new Giffy("id ${DateTime.now()}", "url", "title", "url_image", "height", "width", "username", "avatar_url", "display_name");
    box.put("giffy.id",giffy);
    print("Ma box contient exactement : ${box.length}");
    return box;
  }
}
