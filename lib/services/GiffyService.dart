import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:myfunnygif/models/Giffy.dart';

final String _APIURL = "https://api.giphy.com/v1";
final String _APIKEY = "cD7vqXsN4E5oT898ub5XsDKtRJY82hCs";

Future<List<Giffy>> getSearch({String q,int limit}) async {
  List<Giffy> giffyResult = [];
  try{
    var response = await http.get("$_APIURL/gifs/search?api_key=$_APIKEY&q=$q&limit=${25 + limit}&offset=${limit}&rating=g&lang=en");
    var data = jsonDecode(response.body);
    final listJson =  data["data"] as List<dynamic>;
    listJson.forEach((element) {
         giffyResult.add(Giffy.fromJson(element));
       });
  }catch(err){
    print(err.toString());
  }
  return giffyResult;
}

Future getTrending() async{
  List<Giffy> giffyResult = [];
  try{
    var response = await http.get("$_APIURL/stickers/trending?api_key=$_APIKEY&limit=20&rating=g");
    var data = jsonDecode(response.body);
    final listJson =  data["data"] as List<dynamic>;
    listJson.forEach((element) {
      giffyResult.add(Giffy.fromJson(element));
    });
  }catch(err){
    print(err.toString());
  }
  return giffyResult;
}

