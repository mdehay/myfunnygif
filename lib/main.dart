import 'package:flutter/material.dart';
import 'package:myfunnygif/models/GiffyDatabaseModel.dart';
import 'package:myfunnygif/models/GiffySearchModel.dart';
import 'package:myfunnygif/models/GiffyHistoriqueModel.dart';
import 'package:provider/provider.dart';

import 'Screen/HomePage/HomePage.dart';
import 'hivetest.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => GiffyHistoriqueModel()),
        ChangeNotifierProvider(create: (context) => GiffySearchModel()),
        ChangeNotifierProvider(create: (context) => GiffyDatabaseModel()),
      ],
      child: MyApp(),
    ),
  );

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GIFFY | Search All Funny GIFs ',
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
       home: HomePage(),
      //home: HiveMVC(),
      debugShowCheckedModeBanner: false,
    );
  }
}


