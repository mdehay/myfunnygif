import 'package:flutter/material.dart';
import 'package:myfunnygif/Screen/FavoritePage/FavoritePage.dart';
import 'package:myfunnygif/Screen/GiffyDescriptionPage/GiffyDescriptionPage.dart';
import 'package:myfunnygif/Screen/HomePage/CustomSearchDelegate.dart';
import 'package:myfunnygif/models/Giffy.dart';
import 'package:myfunnygif/services/GiffyService.dart';
import 'package:myfunnygif/services/SizeController.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("My Treading")),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () =>
                showSearch(context: context, delegate: CustomSearchDelegate()),
          ),
          IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () => Navigator.of(context).push(_AnimationRouteToFavorite()),
          ),
        ],
      ),
      body: FutureBuilder(
        future: getTrending(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return GridView.count(
              crossAxisCount:
                  getGridSizeByPlatform(MediaQuery.of(context).size.width),
              children: List.generate(snapshot.data.length, (index) {
                Giffy currentGiffy = snapshot.data[index];
                return GestureDetector(
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GiffyDescriptionPage(
                          giffy: currentGiffy,
                        ),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Card(
                        elevation: 20.0,
                        child: Hero(
                          tag: currentGiffy.id,
                          child: Image.network(
                            currentGiffy.url_image,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }),
            );
          } else if (snapshot.hasError) {
            return Text("Error");
          } else {
            return CircularProgressIndicator(
              value: 2.0,
            );
          }
        },
      ),
    );
  }

  Route _AnimationRouteToFavorite() {
    GiffyDescriptionPage().runtimeType;
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => FavoritePage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}
