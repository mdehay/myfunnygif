import 'package:flutter/material.dart';
import 'package:myfunnygif/Screen/CustomSeachPage/CustomSearchPage.dart';
import 'package:myfunnygif/models/GiffyHistoriqueModel.dart';
import 'package:provider/provider.dart';

class CustomSearchDelegate extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
    throw UnimplementedError();
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () => close(context, null),
    );
    throw UnimplementedError();
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) query = "cat";
    Provider.of<GiffyHistoriqueModel>(context, listen: false).add(query);
    return CustomSearchPage(
      query: query,
    );
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Consumer<GiffyHistoriqueModel>(
      builder: (context, model, child) {
        return ListView.builder(
          padding: EdgeInsets.all(5),
          itemCount: model.historiques.length,
          itemBuilder: (context, index) {
            if (model.historiques[index]
                .toUpperCase()
                .contains(query.toUpperCase())) {
              return ListTile(
                title: Text(model.historiques[index]),
                onTap: () => query = model.historiques[index],
                trailing: IconButton(
                  icon: Icon(Icons.clear),
                  onPressed: () => model.remove(model.historiques[index]),
                ),
              );
            } else {
              return null;
            }
          },
        );
      },
    );

    throw UnimplementedError();
  }
}
