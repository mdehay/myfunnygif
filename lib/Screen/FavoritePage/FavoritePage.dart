import 'package:flutter/material.dart';
import 'package:myfunnygif/Screen/GiffyDescriptionPage/GiffyDescriptionPage.dart';
import 'package:myfunnygif/models/Giffy.dart';
import 'package:myfunnygif/models/GiffyDatabaseModel.dart';
import 'package:provider/provider.dart';

class FavoritePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          child: Consumer<GiffyDatabaseModel>(
            builder: (context, BDD, child) {
              return FutureBuilder<List<Giffy>>(
                future: BDD.SelectAllGiffys(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        return Container(
                          height: 200,
                          width: MediaQuery.of(context).size.width,
                          child: Card(
                            elevation: 20.0,
                            child: FlatButton(
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => GiffyDescriptionPage(
                                      giffy: snapshot.data[index],
                                    ),
                                  )),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Container(
                                    height: 75,
                                    width: 100,
                                    child: Hero(
                                        tag: snapshot.data[index].id,
                                        child: Image.network(
                                          snapshot.data[index].url_image,
                                          fit: BoxFit.fill,
                                        ),
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(snapshot.data[index].display_name),
                                      Text("@${snapshot.data[index].username}"),
                                    ],
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.clear),
                                    onPressed: () =>
                                        Provider.of<GiffyDatabaseModel>(context,
                                                listen: false)
                                            .delete(snapshot.data[index].id),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  } else {
                    return Text("You don't have Favorite giffy in your list ");
                  }
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
