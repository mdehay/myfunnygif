import 'package:flutter/material.dart';
import 'package:http/http.dart' as HTTP;
import 'package:flutter/foundation.dart';
import 'package:myfunnygif/models/Giffy.dart';
import 'package:myfunnygif/models/GiffyDatabaseModel.dart';
import 'package:provider/provider.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';

class GiffyDescriptionPage extends StatelessWidget {
  final Giffy giffy;

  const GiffyDescriptionPage({Key key, this.giffy}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("GIF")),
      ),
      body: Center(
        child: Container(
          color: Colors.black,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                giffy.title,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 3,
                    child: Hero(
                      tag: giffy.id,
                      child: Image.network(
                        giffy.url_image,
                        fit: BoxFit.fill,
                      ),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RowProfil(),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () async{
                            await Provider.of<GiffyDatabaseModel>(context,listen: false).insert(giffy);
                          },
                          icon: Icon(
                            Icons.favorite,
                            color: Colors.white,
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.share,
                            color: Colors.white,
                          ),
                          onPressed: () async {
                            try {
                              var response = await HTTP.get(giffy.url_image);
                              await WcFlutterShare.share(
                                sharePopupTitle: 'share',
                                subject: 'Funny Gif O M G !',
                                text: 'Woah Dude ! Look at this GIF',
                                fileName: 'share.png',
                                mimeType: 'image/gif',
                                bytesOfFile: response.bodyBytes,
                              );
                            } catch (err) {
                              print(err.toString());
                            }
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Text(giffy.url_image),
              Text(giffy.title),
            ],
          ),
        ),
      ),
    );
  }

  Widget RowProfil() {
    return Row(
      children: [
        Container(
            width: 50,
            height: 50,
            child: Image.network(
              (giffy.avatar_url != null)
                  ? giffy.avatar_url
                  : "https://giphy.com/static/img/giphy_logo_square_social.png",
              fit: BoxFit.fill,
            )),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                giffy.display_name,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              Text(
                "@${giffy.username}",
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

