import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myfunnygif/Screen/GiffyDescriptionPage/GiffyDescriptionPage.dart';
import 'package:myfunnygif/models/Giffy.dart';
import 'package:myfunnygif/models/GiffySearchModel.dart';
import 'package:myfunnygif/services/GiffyService.dart';
import 'package:myfunnygif/services/SizeController.dart';
import 'package:provider/provider.dart';


class CustomSearchPage extends StatefulWidget {
  final String query;

  const CustomSearchPage({Key key, this.query}) : super(key: key);

  @override
  _CustomSearchPageState createState() => _CustomSearchPageState();
}

class _CustomSearchPageState extends State<CustomSearchPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<GiffySearchModel>(
      builder: (context, GSModel, child) {
        if (!GSModel.isExist(q: widget.query)) {
          getSearch(q: widget.query, limit: 0).then((List<Giffy> value) {
            value.forEach((element) {
              GSModel.add(giffy: element, q: widget.query);
            });
          });
          return CircularProgressIndicator(
            value: 2.0,
          );
        }
        return GridView.count(
          crossAxisCount: getGridSizeByPlatform(MediaQuery.of(context).size.width),
          children: List.generate(GSModel.count(q: widget.query) + 1, (index) {
            if (GSModel.count(q: widget.query) == 0) {
              return Text("Aucun gif trouvé");
            } else if (index == GSModel.count(q: widget.query)) {
              return SizedBox.fromSize(
                  size: Size(60, 60),
                  // button width and height
                  child: ClipOval(
                    child: Material(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(200),
                          side: BorderSide(
                            width: 2,
                            color: Colors.black,
                          )),
                      color:Colors.grey[800], // button color
                      child: InkWell(
                        onTap: () {
                          getSearch(
                              q: widget.query,
                              limit: GSModel.giffys[widget.query].length)
                              .then((List<Giffy> value) {
                            value.forEach((element) {
                              GSModel.add(giffy: element, q: widget.query);
                            });
                          });
                        },// button pressed
                        child: Icon(
                          Icons.arrow_circle_down,
                          color: Colors.white,
                          size: 50,
                        ),
                      ),
                    ),
                  ),
                );

            }
            Giffy currentGiffy = GSModel.giffys[widget.query][index];
            return GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => GiffyDescriptionPage(
                      giffy: currentGiffy,
                    ),
                  )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: Card(
                      elevation: 20.0,
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(image: AssetImage("asset/loading.png"),fit: BoxFit.fill),
                        ),
                        child: FutureBuilder(
                          future: _getUrlImg(currentGiffy.url_image),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Hero(tag: currentGiffy.id ,child:snapshot.data);
                            } else if (snapshot.hasError) {
                              return Text("ERROR 404 : No data exist");
                            } else {
                              return Container();
                            }
                          },
                        ),
                      )),
                ),
              ),
            );
          }),
        );
      },
    );
  }
}

Future<Widget> _getUrlImg(String url) async {
 return await Image.network(url,fit: BoxFit.fill,);
}
