import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'Giffy.dart';


class GiffyDatabaseModel extends ChangeNotifier {
  static final _tableName = 'test';
  static final _databaseName = 'giffy_database';
  Database _database;

  GiffyDatabaseModel() {
    init();
  }

  void init() async {
    final databasesPath = await getDatabasesPath();
    String path = join(databasesPath, _databaseName);
    _database = await openDatabase(
      path,
      onCreate: (db, version) async {
        return await db.execute(
            'CREATE TABLE IF NOT EXISTS $_tableName (id TEXT PRIMARY KEY, url TEXT, title TEXT, url_image TEXT, height TEXT, width TEXT, username TEXT, display_name TEXT, avatar_url TEXT)');
      },
      version: 1,
    );
    notifyListeners();
  }

  Future<bool> isAlwaysFavorite(String id) async{
    var value = await _database.query(_tableName,where: "id = ?", whereArgs: [id],);
    if(value.length == 1){
    return true;
    }else{
      return false;
    }
  }

  Future<List<Giffy>> SelectAllGiffys() async {
    final List<Map<String, dynamic>> maps = await _database.query(_tableName);
    return List.generate(maps.length, (i) {
      print(maps[i]);
      return Giffy.fromDatabase(maps[i]);
    });
  }

  Future<void> insert(Giffy giffy) async {
    await _database.insert(
      _tableName,
      giffy.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    notifyListeners();
  }

  Future<void> delete(String id) async {
    await _database.delete(
      _tableName,
      where: "id = ?",
      whereArgs: [id],
    );
    notifyListeners();
  }
}


