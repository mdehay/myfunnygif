import 'package:hive/hive.dart';

@HiveType(typeId: 0)
class Giffy extends HiveObject {

  @HiveField(0)
  final String id;

  @HiveField(1)
  final String url;

  @HiveField(2)
  final String title;

  @HiveField(3)
  final String url_image;

  @HiveField(4)
  final String height;

  @HiveField(5)
  final String width;

  @HiveField(6)
  final String username;

  @HiveField(7)
  final String display_name;

  @HiveField(8)
  final String avatar_url;

  Giffy(this.id, this.url, this.title, this.url_image, this.height, this.width,
      this.username, this.avatar_url, this.display_name);

  Giffy.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        url = json['url'],
        title = json['title'],
        url_image = json["images"]["downsized"]["url"],
        height = json["images"]["downsized"]["height"],
        username = json.containsValue(json["user"])
            ? json["user"]["username"]
            : "FreeUser",
        display_name = json.containsValue(json["user"])
            ? json["user"]["display_name"]
            : "Nobody",
        avatar_url = json.containsValue(json["user"])
            ? json["user"]["avatar_url"]
            : null,
        width = json["images"]["downsized"]["width"];

  Giffy.fromDatabase(Map<String, dynamic> bdd)
      : id = bdd['id'],
        url = bdd['url'],
        title = bdd['title'],
        url_image = bdd["url_image"],
        height = bdd["height"],
        username = bdd["username"],
        display_name = bdd["display_name"],
        avatar_url = bdd["avatar_url"],
        width = bdd["width"];

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'url': url,
      'title': title,
      'url_image': url_image,
      'height': height,
      'width': width,
      'username': username,
      'display_name': display_name,
      'avatar_url': avatar_url,
    };
  }
}
