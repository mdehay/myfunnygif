import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:myfunnygif/models/Giffy.dart';

class GiffySearchModel extends ChangeNotifier{

  Map<String,List<Giffy>> _giffys = new Map<String,List<Giffy>>();

 UnmodifiableMapView<String,List<Giffy>> get giffys =>  UnmodifiableMapView(_giffys);

  int count({String q}){
    return _giffys[q].length;
  }

  bool isExist({String q}){
    return _giffys.containsKey(q);
  }

  void add({Giffy giffy,String q}){
    if(!isExist(q: q)) _giffys[q] = new List<Giffy>();
    _giffys[q].add(giffy);
    notifyListeners();
  }


  void addAll({List<Giffy> giffys,String q}){
    _giffys[q].addAll(giffys);
    notifyListeners();
  }


  void remove({Giffy giffy,String q}){
    _giffys[q].remove(giffy);
    notifyListeners();
  }


  void removeAll({String q}){
    _giffys[q].clear();
    notifyListeners();
  }

  void removeClear(){
    _giffys.clear();
    notifyListeners();
  }

}