import 'dart:collection';

import 'package:flutter/material.dart';

class GiffyHistoriqueModel extends ChangeNotifier{
  Set<String> _historiques = new Set<String>();

  UnmodifiableListView get historiques => UnmodifiableListView(_historiques);

  void add(String value){
    _historiques.add(value);
  }
  
  void remove(String value){
    _historiques.remove(value);
    notifyListeners();
  }

}